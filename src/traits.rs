use std::fmt::Debug;
use std::ops::*;

pub trait Arith: Debug +
    Sized + Copy + Clone +
    PartialEq +
    Add<Output = Self> + AddAssign +
    Sub<Output = Self> + SubAssign +
    Mul<Output = Self> + MulAssign +
    Div<Output = Self> + DivAssign +
    Neg<Output = Self> {

    const ZERO: Self;
    const ONE: Self;
}

pub trait Float: Arith {
    fn sin(self) -> Self;
    fn cos(self) -> Self;
    fn atan2(self, other: Self) -> Self;
    fn sqrt(self) -> Self;
}

impl Arith for f32 {
    const ZERO: Self = 0f32;
    const ONE: Self = 1f32;
}

impl Float for f32 {
    fn sin(self) -> Self { f32::sin(self) }
    fn cos(self) -> Self { f32::cos(self) }
    fn atan2(self, other: Self) -> Self { f32::atan2(self, other) }
    fn sqrt(self) -> Self { f32::sqrt(self) }
}

impl Arith for f64 {
    const ZERO: Self = 0f64;
    const ONE: Self = 1f64;
}

impl Float for f64 {
    fn sin(self) -> Self { f64::sin(self) }
    fn cos(self) -> Self { f64::cos(self) }
    fn atan2(self, other: Self) -> Self { f64::atan2(self, other) }
    fn sqrt(self) -> Self { f64::sqrt(self) }
}