use std::ops::*;
use crate::traits::{Arith, Float};

pub trait ComplexArith: Arith {
    /// The imaginary unit.
    const I: Self;

    /// The underlying floating-point real type.
    type RealType;

    /// Computes the conjugate of this number. `(x + yi)* = x - yi`
    fn conjugate(self) -> Self;

    /// Computes `cis(x) = e^ix = cos(x) + isin(x)`.
    fn cis(x: Self::RealType) -> Self;

    /// Computes the squared magnitude: `|x + yi|^2 = x^2 + y^2`.
    fn magnitude_squared(self) -> Self::RealType;

    /// Computes the magnitude of this complex number.
    fn magnitude(self) -> Self::RealType;

    /// Computes the argument: `arg(x + yi) = atan2(y, x)`.
    fn argument(self) -> Self::RealType;
}

#[derive(Debug)]
#[derive(Copy, Clone, PartialEq)]
pub struct Complex<F> where F: Float {
    pub real: F,
    pub imag: F
}

impl<F> Complex<F> where F: Float {
    /// Initializes a new complex structure.
    fn new(real: F, imag: F) -> Self {
        Self {real, imag}
    }
}

impl<F> ComplexArith for Complex<F> where F: Float {
    const I: Self = Self {real: F::ZERO, imag: F::ONE};
    type RealType = F;

    fn conjugate(self) -> Self {
        Self {
            real: self.real,
            imag: -self.imag
        }
    }

    fn cis(x: Self::RealType) -> Self {
        Self {
            real: x.cos(),
            imag: x.sin()
        }
    }

    fn magnitude_squared(self) -> Self::RealType {
        self.real * self.real + self.imag * self.imag
    }

    fn magnitude(self) -> Self::RealType {
        self.magnitude_squared().sqrt()
    }

    fn argument(self) -> Self::RealType {
        self.imag.atan2(self.real)
    }
}

impl<F> Add for Complex<F> where F: Float {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            real: self.real + rhs.real,
            imag: self.imag + rhs.imag
        }
    }
}

impl<F> Sub for Complex<F> where F: Float {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            real: self.real - rhs.real,
            imag: self.imag - rhs.imag
        }
    }
}

impl<F> Mul for Complex<F> where F: Float {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        let (x, y) = (self.real, self.imag);
        let (u, v) = (rhs.real, rhs.imag);

        Self {
            real: x * u - y * v,
            imag: x * v + y * u
        }
    }
}

impl<F> Div for Complex<F> where F: Float {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        let (x, y) = (rhs.real, rhs.imag);
        let (u, v) = (self.real, self.imag);

        let p = u * x + v * y;
        let q = v * x - u * y;

        Self {
            real: p / rhs.magnitude_squared(),
            imag: q / rhs.magnitude_squared()
        }
    }
}

impl<F> Neg for Complex<F> where F: Float {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self {
            real: -self.real,
            imag: -self.imag,
        }
    }
}

/// Addition of a float (only affects the real part).
impl<F> Add<F> for Complex<F> where F: Float {
    type Output = Self;

    fn add(self, rhs: F) -> Self::Output {
        Self {
            real: self.real + rhs,
            imag: self.imag
        }
    }
}

/// Subtraction of a float (only affects the real part).
impl<F> Sub<F> for Complex<F> where F: Float {
    type Output = Self;

    fn sub(self, rhs: F) -> Self::Output {
        Self {
            real: self.real - rhs,
            imag: self.imag
        }
    }
}

/// Multiplication by a float (scale).
impl<F> Mul<F> for Complex<F> where F: Float {
    type Output = Self;

    fn mul(self, rhs: F) -> Self::Output {
        Self {
            real: self.real * rhs,
            imag: self.imag * rhs
        }
    }
}

/// Division by a float (scale).
impl<F> Div<F> for Complex<F> where F: Float {
    type Output = Self;

    fn div(self, rhs: F) -> Self::Output {
        Self {
            real: self.real / rhs,
            imag: self.imag / rhs
        }
    }
}

impl<F> AddAssign for Complex<F> where F: Float {
    fn add_assign(&mut self, rhs: Self) {
        *self = *self + rhs;
    }
}

impl<F> SubAssign for Complex<F> where F: Float {
    fn sub_assign(&mut self, rhs: Self) {
        *self = *self - rhs;
    }
}

impl<F> MulAssign for Complex<F> where F: Float {
    fn mul_assign(&mut self, rhs: Self) {
        *self = *self * rhs;
    }
}

impl<F> DivAssign for Complex<F> where F: Float {
    fn div_assign(&mut self, rhs: Self) {
        *self = *self / rhs;
    }
}

impl<F> AddAssign<F> for Complex<F> where F: Float {
    fn add_assign(&mut self, rhs: F) {
        *self = *self + rhs;
    }
}

impl<F> SubAssign<F> for Complex<F> where F: Float {
    fn sub_assign(&mut self, rhs: F) {
        *self = *self - rhs;
    }
}

impl<F> MulAssign<F> for Complex<F> where F: Float {
    fn mul_assign(&mut self, rhs: F) {
        *self = *self * rhs;
    }
}

impl<F> DivAssign<F> for Complex<F> where F: Float {
    fn div_assign(&mut self, rhs: F) {
        *self = *self / rhs;
    }
}

impl<F> Arith for Complex<F> where F: Float {
    const ZERO: Self = Self {real: F::ZERO, imag: F::ZERO};
    const ONE: Self = Self {real: F::ONE, imag: F::ZERO};
}

pub type Complex32 = Complex<f32>;
pub type Complex64 = Complex<f64>;

#[cfg(test)]
mod tests {
    use crate::complex::*;

    #[test]
    fn test_conjugate() {
        let x = Complex::new(3f32, 5f32);
        assert_eq!(x.conjugate(), Complex::new(3f32, -5f32));
    }

    #[test]
    fn test_cis() {
        assert_eq!(Complex::cis(5f32), Complex::new(0.2836622f32, -0.9589243f32))
    }

    #[test]
    fn test_magnitude_squared() {
        let x = Complex::new(3f32, 5f32);
        assert_eq!(x.magnitude_squared(), 34f32);
    }

    #[test]
    fn test_magnitude() {
        let x = Complex::new(3f32, 4f32);
        assert_eq!(x.magnitude(), 5f32);
    }

    #[test]
    fn test_argument() {
        let x = Complex::new(3f32, -4f32);
        assert_eq!(x.argument(), -0.9272952f32);
    }

    #[test]
    fn test_add() {
        let x = Complex::new(3f32, -4f32);
        let y = Complex::new(-5f32, 7f32);
        assert_eq!(x + y, Complex::new(-2f32, 3f32));
    }

    #[test]
    fn test_sub() {
        let x = Complex::new(3f32, -4f32);
        let y = Complex::new(-5f32, 7f32);
        assert_eq!(x - y, Complex::new(8f32, -11f32));
    }

    #[test]
    fn test_mul() {
        let x = Complex::new(3f32, -4f32);
        let y = Complex::new(-5f32, 7f32);
        assert_eq!(x * y, Complex::new(13f32, 41f32));
    }

    #[test]
    fn test_div() {
        let x = Complex::new(3f32, -4f32);
        let y = Complex::new(-5f32, 7f32);
        assert_eq!(x / y, Complex::new(-0.5810811f32, -0.013513514f32));
    }

    #[test]
    fn test_neg() {
        let x = Complex::new(3f32, -5f32);
        assert_eq!(-x, Complex::new(-3f32, 5f32));
    }

    #[test]
    fn test_add_real() {
        let x = Complex::new(3f32, -5f32);
        assert_eq!(x + 5f32, Complex::new(8f32, -5f32));
    }

    #[test]
    fn test_sub_real() {
        let x = Complex::new(3f32, -5f32);
        assert_eq!(x - 5f32, Complex::new(-2f32, -5f32));
    }

    #[test]
    fn test_scale_mul() {
        let x = Complex::new(3f32, -5f32);
        assert_eq!(x * 2f32, Complex::new(6f32, -10f32))
    }

    #[test]
    fn test_scale_div() {
        let x = Complex::new(3f32, -5f32);
        assert_eq!(x / 2f32, Complex::new(1.5f32, -2.5f32))
    }

    #[test]
    fn test_add_assign() {
        let mut x = Complex::new(3f32, -4f32);
        let y = Complex::new(-5f32, 7f32);
        x += y;
        assert_eq!(x, Complex::new(-2f32, 3f32));
    }

    #[test]
    fn test_sub_assign() {
        let mut x = Complex::new(3f32, -4f32);
        let y = Complex::new(-5f32, 7f32);

        x -= y;
        assert_eq!(x, Complex::new(8f32, -11f32));
    }

    #[test]
    fn test_add_real_assign() {
        let mut x = Complex::new(3f32, -5f32);
        x += 5f32;
        assert_eq!(x, Complex::new(8f32, -5f32));
    }

    #[test]
    fn test_sub_real_assign() {
        let mut x = Complex::new(3f32, -5f32);
        x -= 5f32;
        assert_eq!(x, Complex::new(-2f32, -5f32));
    }

    #[test]
    fn test_mul_assign() {
        let mut x = Complex::new(3f32, -4f32);
        let y = Complex::new(-5f32, 7f32);
        x *= y;
        assert_eq!(x, Complex::new(13f32, 41f32));
    }

    #[test]
    fn test_div_assign() {
        let mut x = Complex::new(3f32, -4f32);
        let y = Complex::new(-5f32, 7f32);
        x /= y;
        assert_eq!(x, Complex::new(-0.5810811f32, -0.013513514f32));
    }

    #[test]
    fn test_scale_mul_assign() {
        let mut x = Complex::new(3f32, -5f32);
        x *= 2f32;
        assert_eq!(x, Complex::new(6f32, -10f32))
    }

    #[test]
    fn test_scale_div_assign() {
        let mut x = Complex::new(3f32, -5f32);
        x /= 2f32;
        assert_eq!(x, Complex::new(1.5f32, -2.5f32))
    }
}